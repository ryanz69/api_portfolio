<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\HttpFoundation\Request;

/**
 * @Route("/rest/mail", name="rest_mail")
 */
class MailController extends Controller
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods="POST")
     */
    public function sendMail(Request $request, \Swift_Mailer $mailer)
    {
        $content = json_decode($request->getContent(), true);

        $message = (new \Swift_Message('portfolio message'))
            ->setFrom($content['mail'])
            ->setTo('ryan.zegar.01@gmail.com')
            ->setBody('NAME:' . "\n" . $content['name'] . "\n" . 'MAIL:' . "\n" . $content['mail'] . "\n" . 'MESSAGE:' . "\n" . $content['message']);

            if ($mailer->send($message))
            {
              $result = "send";
            }
            else
            {
              $result = "failed";
            }

        $jsonContent = $this->serializer->serialize($result, 'json');
        return JsonResponse::fromJsonString($jsonContent);
    }
}
