<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\ProjetRepository;
use Symfony\Component\HttpFoundation\Request;
use App\Entity\Projet;
use Doctrine\Common\Persistence\ObjectManager;
use App\Form\ProjetType;
use Symfony\Component\Form\FormErrorIterator;

/**
* @Route("/rest/projets", name="rest_projets")
*/
class ProjetsController extends AbstractController
{
    private $serializer;

    public function __construct(SerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    /**
     * @Route("/", methods="GET")
     */
    public function index(ProjetRepository $projets)
    {

        $data = $projets->findAll();

        $jsonContent = $this->serializer->serialize($data, 'json');

        return JsonResponse::fromJsonString($jsonContent);
    }

    /**
     * @Route("/admin/", methods="POST")
     */
    public function add(Request $request, ObjectManager $manager)
    {
        $content = json_decode($request->getContent(), true);

        $project = new Projet();

        $project->setName($content["name"]);
        $project->setBase64Image($content["image"]);
        $project->setDescription($content["description"]);
        $project->setLien($content["lien"]);
        $project->setGitlab($content["gitlab"]);

        $manager->persist($project);
        $manager->flush();
        return new Response();
    }

    /**
     * @Route("/{projet}", methods="GET")
     */
    public function single(Projet $projet) {
        $json = $this->serializer->serialize($projet, "json");
        return JsonResponse::fromJsonString($json);
    }

    /**
     * @Route("/admin/{projet}", methods="DELETE")
     */
    public function remove(Projet $projet, ObjectManager $manager) {
        $manager->remove($projet);
        $manager->flush();
        return new Response();
    }

    /**
     * @Route("/admin/{projet}", methods="PUT")
     */
    public function update(Projet $projet, ObjectManager $manager, Request $request) {
        $content = json_decode($request->getContent(), true);

        $projet->setName($content["name"]);
        if($content["image"] != null) {
            dump($projet->getImage());
            $projet->setBase64Image($content["image"]);
        }
        $projet->setDescription($content["description"]);
        $projet->setGitlab($content["gitlab"]);
        $projet->setLien($content["lien"]);
        $manager->persist($projet);
        $manager->flush();
        return new Response();
    }

    private function listFormErrors(FormErrorIterator $errors) {
        $list = [];
        forEach($errors as $error) {
            $list[] = $error->getMessage();
        }
        return $list;
    }
}